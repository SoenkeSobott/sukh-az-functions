﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

using Microsoft.Azure.Storage.Blob;

using System.Security.Cryptography;
using Microsoft.Azure.Storage;
using System.Collections.Generic;

namespace query_service
{

    public static class eventFeeds
    {

        [FunctionName("AddEventFeedEntry")]
        public static async Task<IActionResult> RunAddEventFeedEntry([HttpTrigger(AuthorizationLevel.Function, "post", Route = "Event/{eventID}/eventFeed")]HttpRequest req, String eventID, ILogger log)
        {
            string feedEntryId;
            byte[] image;
            string createdBy;
            DateTime createdAt;
        
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            feedEntryId = data?.feedEntryId;
            image = data?.image;
            createdBy = data?.createdBy;
            createdAt = data?.createdAt;

            try
            {
                // Connect to MongoDBAtlas
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<events.Event>("events");
                var userCollection = database.GetCollection<users.User>("users");

                // Check if event exists
                bool eventExists = collection.Find(_ => _.eventID == eventID).Any();
                bool userExists = userCollection.Find(_ => _.username == createdBy).Any();

                if (eventExists && userExists)
                {
                    var currentEvent = collection.Find(_ => _.eventID == eventID).First();
                    var updatedEventFeedEntries = currentEvent.eventFeedEntries;

                    // Upload profile image to Azure Blob Storage with return of URL
                    string imageURL = await uploadImageDataToBlobStorageAsync(image, feedEntryId);

                    // Create newEventFeedEntry with values
                    var newEventFeedEntry = new events.EventFeedEntry
                    {
                        feedEntryId = feedEntryId,
                        imageURL = imageURL,
                        createdBy = createdBy,
                        createdAt = createdAt
                    };

                    if (!updatedEventFeedEntries.Contains(newEventFeedEntry))
                    {
                        updatedEventFeedEntries.Add(newEventFeedEntry);
                    }

                    // Insert newEventFeedEntry into Event
                    var filter = Builders<events.Event>.Filter.Eq(x => x.eventID, eventID);
                    var update = Builders<events.Event>.Update.Set(x => x.eventFeedEntries, updatedEventFeedEntries);

                    var t = collection.UpdateOneAsync(filter, update).Result;

                    // Update credit of user
                    var user = userCollection.Find(_ => _.username == createdBy).First();

                    var monthlyCredit = user.monthlyCredit;
                    monthlyCredit = monthlyCredit + 1;

                    var totalCredit = user.totalCredit;
                    totalCredit = totalCredit + 1;

                    var userFilter = Builders<users.User>.Filter.Eq(_ => _.username, createdBy);
                    var userUpdate = Builders<users.User>.Update.Set(_ => _.monthlyCredit, monthlyCredit).Set(_ => _.totalCredit, totalCredit);

                    await userCollection.UpdateOneAsync(userFilter, userUpdate);

                    // Return 200 
                    var result = new OkObjectResult($"MongoDB sukh.event: Inserted {feedEntryId} to feedEntries");
                    result.StatusCode = StatusCodes.Status200OK;
                    return result;
                }
                else
                {
                    var result = new BadRequestObjectResult($"MongoDB sukh.event: eventID not exists");
                    result.StatusCode = StatusCodes.Status404NotFound;
                    return result;
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
        }


        [FunctionName("RemoveEventFeedEntry")]
        public static IActionResult RunRemoveEventFeedEntry([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "Event/{eventID}/eventFeed/{feedEntryId}")]HttpRequest req, String eventID, String feedEntryId, ILogger log)
        {

            try
            {
                // Connect to MongoDBAtlas
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<events.Event>("events");
                var userCollection = database.GetCollection<users.User>("users");

                bool exists = collection.Find(_ => _.eventID == eventID).Any();

                if (exists)
                {

                    var eve = collection.Find(_ => _.eventID == eventID).First();
                    var feedEntry = eve.eventFeedEntries.Find(_ => _.feedEntryId == feedEntryId);

                    // Update credit of user
                    var user = userCollection.Find(_ => _.username == feedEntry.createdBy).First();

                    var monthlyCredit = user.monthlyCredit;
                    monthlyCredit = monthlyCredit - 1;

                    var totalCredit = user.totalCredit;
                    totalCredit = totalCredit - 1;

                    var userFilter = Builders<users.User>.Filter.Eq(_ => _.username, feedEntry.createdBy);
                    var userUpdate = Builders<users.User>.Update.Set(_ => _.monthlyCredit, monthlyCredit).Set(_ => _.totalCredit, totalCredit);

                    userCollection.UpdateOneAsync(userFilter, userUpdate);


                    // Delete feed entry
                    var update = Builders<events.Event>.Update.PullFilter(p => p.eventFeedEntries, f => f.feedEntryId == feedEntryId);

                    var result = collection.FindOneAndUpdateAsync(p => p.eventID == eventID, update).Result;

                    deleteImageDataFromBlobStorage(feedEntryId);

                    

                    // Return 200 
                    var response = new OkObjectResult($"MongoDB sukh.event: Removed {feedEntryId} from feedEntries");
                    response.StatusCode = StatusCodes.Status200OK;
                    return response;
                }
                else
                {
                    var result = new BadRequestObjectResult($"MongoDB sukh.event: eventID not exists");
                    result.StatusCode = StatusCodes.Status404NotFound;
                    return result;
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error deleting data - " + e.Message);

            }


        }

        public static void addCredit(String username)
        {

        }

        public static async Task<string> uploadImageDataToBlobStorageAsync(byte[] image, string feedEntryId)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("event-feed-images");
                await cloudBlobContainer.CreateIfNotExistsAsync();

                // Create a fileName and append guid
                string fileName = "EventFeedEntry_" + feedEntryId + ".png";

                // Get a reference to the blob address, then upload the file to the blob.
                // Use the value of fileName for the blob name.
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
                int length = image.Length;
                cloudBlockBlob.UploadFromByteArray(image, 0, length);
                return cloudBlobContainer.StorageUri.PrimaryUri + "/" + fileName;
            }
            else
            {
                return "defaulturl";
            }
        }

        public static bool deleteImageDataFromBlobStorage(string feedEntryId)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {

                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("event-feed-images");
                string completeFileName = "EventFeedEntry_" + feedEntryId + ".png";
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(completeFileName);
                cloudBlockBlob.Delete();

                return true;

            }
            else
            {
                return false;
            }
        }
    }
}

