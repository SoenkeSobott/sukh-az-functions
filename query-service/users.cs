using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;

using Microsoft.Azure.Storage.Blob;

using System.Security.Cryptography;
using Microsoft.Azure.Storage;
using System.Collections.Generic;

namespace query_service
{

    public static class users
    {
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        [FunctionName("CreateUser")]
        public static async Task<IActionResult> RunCreateUser([HttpTrigger(AuthorizationLevel.Function, "post", Route = "User")]HttpRequest req, ILogger log)
        {
            string username; // IS unique -> equals ID
            string name;
            string mail;
            string password;
            string salt = generateSalt(rngCsp, 32);
            DateTime createdAt;
            string role;
            byte[] profileImage;
            int monthlyCredit;
            int totalCredit;

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            username = data?.username;
            name = data?.name;
            mail = data?.mail;
            password = data?.password;
            createdAt = data?.createdAt;
            role = data?.role;
            profileImage = data?.profileImage;
            monthlyCredit = data?.monthlyCredit;
            totalCredit = data?.totalCredit;

            try
            {
                // Connect to MongoDBAtlas
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                // Check if user already exists
                bool exists = collection.Find(_ => _.username == username).Any();

                if (!exists)
                {
                    // Upload profile image to Azure Blob Storage with return of URL
                    string profileImageURL = await uploadImageDataToBlobStorageAsync(profileImage, username);

                    // Create hashed password with salt
                    var passwordWithSalt = password + salt;
                    var hashedPasswordWithSalt = BCrypt.Net.BCrypt.HashPassword(passwordWithSalt);

                    // Create newUser with values
                    var newUser = new User
                    {
                        Id = new ObjectId(),
                        username = username,
                        name = name,
                        mail = mail,
                        password = hashedPasswordWithSalt,
                        salt = salt,
                        createdAt = createdAt,
                        role = role,
                        followers = new List<string>(),
                        following = new List<string>(),
                        profileImageURL = profileImageURL,
                        invitations = new List<string>(),
                        attendingEvents = new List<string>(),
                        reports = new List<Report>(),
                        monthlyCredit = monthlyCredit,
                        totalCredit = totalCredit
                    };

                    // Insert newUser into MongoDB
                    await collection.InsertOneAsync(newUser);
                    // Return 201 
                    var result = new OkObjectResult($"MongoDB sukh.users: Inserted {username}");
                    result.StatusCode = StatusCodes.Status201Created;
                    return result;
                }
                else
                {
                    var result = new OkObjectResult($"MongoDB sukh.users: Violating unique constraint - username already exists");
                    result.StatusCode = StatusCodes.Status409Conflict;
                    return result;
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
        }

        [FunctionName("UpdateUser")]
        public static async Task<IActionResult> RunUpdateUser([HttpTrigger(AuthorizationLevel.Function, "post", Route = "User/update")]HttpRequest req, ILogger log)
        {
            string username; // IS unique -> equals ID
            string name;
            string mail;
            string password;
            //string salt = generateSalt(rngCsp, 32);
            string createdAt;
            string role;
            byte[] profileImage;

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            username = data?.username;
            name = data?.name;
            mail = data?.mail;
            password = data?.password;
            createdAt = data?.createdAt;
            role = data?.role;
            profileImage = data?.profileImage;

            try
            {
                // Connect to MongoDBAtlas
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                // Check if user exists
                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {

                    // Upload profile image to Azure Blob Storage with return of URL
                    string profileImageURL = await uploadImageDataToBlobStorageAsync(profileImage, username);


                    var existingUser = collection.Find(_ => _.username == username).First();
                    var filter = Builders<User>.Filter.Eq(s => s.username, username);

                    //Fill our updatedUser
                    var updatedUser = new User
                    {
                        Id = existingUser.Id,
                        username = existingUser.username,
                        name = existingUser.name,
                        mail = existingUser.mail,
                        password = existingUser.password,
                        salt = existingUser.salt,
                        createdAt = existingUser.createdAt,
                        role = existingUser.role,
                        profileImageURL = profileImageURL,
                        followers = existingUser.followers,
                        following = existingUser.following,
                        invitations = existingUser.invitations,
                        attendingEvents = existingUser.attendingEvents,
                        reports = existingUser.reports,
                        monthlyCredit = existingUser.monthlyCredit,
                        totalCredit = existingUser.totalCredit
                    };

                    await collection.ReplaceOneAsync(filter, updatedUser);
                    var result = new OkObjectResult($"MongoDB sukh.users: Updated {username}");
                    result.StatusCode = StatusCodes.Status200OK;
                    return result;
                }
                else
                {
                    return new BadRequestObjectResult("username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
        }

        [FunctionName("AuthenticateUser")]
        public static async Task<IActionResult> RunAuthenticateUser([HttpTrigger(AuthorizationLevel.Function, "post", Route = "User/auth")]HttpRequest req, ILogger log)
        {
            try
            {
                string username; // IS unique -> equals ID
                string password;

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                dynamic data = JsonConvert.DeserializeObject(requestBody);

                username = data?.username;
                password = data?.password;

                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");
          
                // Check if user exists
                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var returnUser = collection.Find(_ => _.username == username).First();

                    // Create hashed password with salt
                    var passwordWithSalt = password + returnUser.salt;
                    var passwordIsCorrect = BCrypt.Net.BCrypt.Verify(passwordWithSalt, returnUser.password);

                    if (passwordIsCorrect)
                    {
                        // 
                        returnUser.password = "";
                        returnUser.salt = "";

                        return (ActionResult)new OkObjectResult(returnUser);
                    }
                    else
                    {
                        return new BadRequestObjectResult("Error authenticating user");
                    }
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetUserByName")]
        public static IActionResult RunGetUserByName([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                var returnUser = collection.Find(_ => _.username == username).First();

                return (ActionResult)new OkObjectResult(returnUser);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetUsers")]
        public static IActionResult RunGetUsers([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/search/{searchTerm}")]HttpRequest req, String username, String searchTerm, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                // Check for blocked users
                bool exists = collection.Find(_ => _.username == username).Any();
                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    var blockedUsers = user.blockedUsers;

                    var filter = Builders<User>.Filter.Regex(_ => _.username, new BsonRegularExpression(searchTerm, "i"));
                    filter = filter & Builders<User>.Filter.Nin(_ => _.username, blockedUsers);
                    filter = filter & Builders<User>.Filter.AnyNe(_ => _.blockedUsers, username);

                    var matchingUsers = collection.Find(filter).ToList();

                    return (ActionResult)new OkObjectResult(matchingUsers);
                } else
                {
                    return new BadRequestObjectResult($"User with username '{username}' does not exist.");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("DeleteUserByName")]
        public static IActionResult RunDeleteUserByName([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "User/{username}")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");
                var eventCollection = database.GetCollection<events.Event>("events");

                // Check if user exists
                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    // Get user
                    var user = collection.Find(_ => _.username == username).First();

                    // delete all invitations in events from the user
                    foreach (String eventID in user.invitations)
                    {
                        // Check if event exists
                        bool eventExists = eventCollection.Find(_ => _.eventID == eventID).Any();
                        if (exists)
                        {
                            var eve = eventCollection.Find(_ => _.eventID == eventID).First();
                            var invitedUsers = eve.invitedUsers;

                            if (invitedUsers.Contains(username))
                            {
                                invitedUsers.Remove(username);
                            }
                            var eventFilter = Builders<events.Event>.Filter.Eq(x => x.eventID, eventID);
                            var eventUpdate = Builders<events.Event>.Update.Set(x => x.invitedUsers, invitedUsers);

                            eventCollection.UpdateOneAsync(eventFilter, eventUpdate);
                        }
                    }

                    // delete all going in events from the user
                    foreach (String eventID in user.attendingEvents)
                    {
                        // Check if event exists
                        bool eventExists = eventCollection.Find(_ => _.eventID == eventID).Any();
                        if (exists)
                        {
                            var eve = eventCollection.Find(_ => _.eventID == eventID).First();
                            var goingUsers = eve.goingUsers;

                            if (goingUsers.Contains(username))
                            {
                                goingUsers.Remove(username);
                            }

                            var eventFilter = Builders<events.Event>.Filter.Eq(x => x.eventID, eventID);
                            var eventUpdate = Builders<events.Event>.Update.Set(x => x.goingUsers, goingUsers);

                            eventCollection.UpdateOneAsync(eventFilter, eventUpdate);
                        }
                    }

                    // delete all following from the user
                    foreach (String userName in user.followers)
                    {
                        // Check if users exists
                        bool userExists = collection.Find(_ => _.username == userName).Any();
                        if (userExists)
                        {
                            var userOne = collection.Find(_ => _.username == userName).First();
                            var following = userOne.following;

                            if (following.Contains(username))
                            {
                                following.Remove(username);
                            }

                            var userFilter = Builders<User>.Filter.Eq(x => x.username, userName);
                            var userUpdate = Builders<User>.Update.Set(x => x.following, following);

                            collection.UpdateOneAsync(userFilter, userUpdate);
                        }
                    }

                    // TODO: find way to delete all events of user (maybe call other azure function or copy method)
                    // get events and set owner to unknown
                    var eventsOfUser = eventCollection.Find(_ => _.createdBy == username).ToList();
                    foreach(events.Event e in eventsOfUser)
                    {
                        var eve = eventCollection.Find(_ => _.eventID == e.eventID).First();

                        if (eve.createdBy == username)
                        {
                            var eventFilter = Builders<events.Event>.Filter.Eq(x => x.eventID, e.eventID);
                            var eventUpdate = Builders<events.Event>.Update.Set(x => x.createdBy, "-");

                            eventCollection.UpdateOneAsync(eventFilter, eventUpdate);
                        }
                    }
                    
                    // Delete user image from Azure Blob Storage
                    deleteImageDataFromBlobStorage(username);

                    // Delete User from MongoDBAtlas
                    collection.DeleteOne(_ => _.username == username);
                    return (ActionResult)new OkObjectResult($"MongoDB sukh.users: Deleted {username}");
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error deleting data - " + e.Message);
            }
        }

        [FunctionName("GetNotificationsOfUser")]
        public static IActionResult RunGetNotificationsOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/notifications")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();

                    var newFollowers = 0;
                    var eventInvitations = user.invitations.Count;


                    //Fill notifications
                    var notifications = new Notifications
                    {
                        newFollowers = newFollowers,
                        eventInvitations = eventInvitations
                    };

                    return (ActionResult)new OkObjectResult(notifications);
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting notifications of user - " + e.Message);
            }
        }

        [FunctionName("AddFollower")]
        public static async Task<IActionResult> RunAddFollower([HttpTrigger(AuthorizationLevel.Function, "put", Route = "User/{username}/follower/{requestingUsername}")]HttpRequest req, String username, String requestingUsername, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool UserOneExists = collection.Find(_ => _.username == username).Any();
                bool UserTwoExists = collection.Find(_ => _.username == requestingUsername).Any();

                if (!username.Equals(requestingUsername))
                {
                    if (UserOneExists && UserTwoExists)
                    {
                        var user = collection.Find(_ => _.username == username).First();
                        var requestingUser = collection.Find(_ => _.username == requestingUsername).First();

                        // Check if the user blocked the requesting user
                        if (user.blockedUsers.Contains(requestingUser.username))
                        {
                            var blockedResult = new OkObjectResult($"Requesting user '{requestingUsername}' is blocked by '{username}'");
                            blockedResult.StatusCode = StatusCodes.Status403Forbidden;
                            return blockedResult;
                        }

                        // Check if the requesting user blocked the user
                        if (requestingUser.blockedUsers.Contains(user.username))
                        {
                            var blockedResult = new OkObjectResult($"Requesting user '{requestingUsername}' blocked the user '{username}'");
                            blockedResult.StatusCode = StatusCodes.Status403Forbidden;
                            return blockedResult;
                        }

                        // Add follower to user
                        var followers = user.followers;

                        if (!followers.Contains(requestingUsername))
                        {
                            followers.Add(requestingUsername);
                        }

                        var filter = Builders<User>.Filter.Eq(x => x.username, username);
                        var update = Builders<User>.Update.Set(x => x.followers, followers);

                        // Add following in requesting user
                        var following = requestingUser.following;
                        if (!following.Contains(username))
                        {
                            following.Add(username);
                        }

                        var filterTwo = Builders<User>.Filter.Eq(x => x.username, requestingUsername);
                        var updateTwo = Builders<User>.Update.Set(x => x.following, following);

                        // Update both users
                        var resultOne = collection.UpdateOneAsync(filter, update).Result;
                        var resultTwo = collection.UpdateOneAsync(filterTwo, updateTwo).Result;

                        // Return 
                        var res = new OkObjectResult($"MongoDB: sukh.users: Added {requestingUsername} as follower to {username}");
                        res.StatusCode = StatusCodes.Status200OK;
                        return res;
                    }
                    else
                    {
                        return new BadRequestObjectResult("Username not found");
                    }
                } else
                {
                    return new BadRequestObjectResult("Cannot follow yourself");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
        }

        [FunctionName("RemoveFollower")]
        public static async Task<IActionResult> RunRemoveFollower([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "User/{username}/follower/{removeUsername}")]HttpRequest req, String username, String removeUsername, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool UserOneExists = collection.Find(_ => _.username == username).Any();
                bool UserTwoExists = collection.Find(_ => _.username == removeUsername).Any();

                if (UserOneExists && UserTwoExists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    var deletingUser = collection.Find(_ => _.username == removeUsername).First();

                    // Remove follower from user
                    var followers = user.followers;
                    followers.Remove(removeUsername);

                    var filter = Builders<User>.Filter.Eq(x => x.username, username);
                    var update = Builders<User>.Update.Set(x => x.followers, followers);

                    // Remove following from removeUsername
                    var following = deletingUser.following;
                    following.Remove(username);

                    var filterTwo = Builders<User>.Filter.Eq(x => x.username, removeUsername);
                    var updateTwo = Builders<User>.Update.Set(x => x.following, following);

                    // Update both users
                    var result = collection.UpdateOneAsync(filter, update).Result;
                    var r = collection.UpdateOneAsync(filterTwo, updateTwo).Result;

                    // return
                    var res = new OkObjectResult($"MongoDB: sukh.users: Deleted follower {removeUsername} from {username}");
                    res.StatusCode = StatusCodes.Status200OK;
                    return res;
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
        }

        [FunctionName("GetFollowersOfUser")]
        public static IActionResult RunGetFollowersOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/follower")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    List<User> followers = new List<User>();

                    foreach (String userName in user.followers)
                    {
                        var follower = collection.Find(_ => _.username == userName).First();
                        followers.Add(follower);
                    }
                    return (ActionResult)new OkObjectResult(followers);
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting followers - " + e.Message);
            }
        }

        [FunctionName("GetFollowingOfUser")]
        public static IActionResult RunGetFollowingOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/following")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    List<User> following = new List<User>();

                    foreach (String userName in user.following)
                    {
                        var follower = collection.Find(_ => _.username == userName).First();
                        following.Add(follower);
                    }
                    return (ActionResult)new OkObjectResult(following);
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting following - " + e.Message);
            }
        }

        [FunctionName("GetInvitationsOfUser")]
        public static IActionResult RunGetInvitationsOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/invitations")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");
                var eventCollection = database.GetCollection<events.Event>("events");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    var invitations = new List<events.Event>();

                    foreach (String invitation in user.invitations)
                    {
                        // TODO: look for better methods to find in list
                        bool eventExists = eventCollection.Find(_ => _.eventID == invitation).Any();
                        if (eventExists)
                        {
                            var eve = eventCollection.Find(_ => _.eventID == invitation).First();
                            invitations.Add(eve);
                        } else
                        {
                            // If event is not existing -> remove event from invitations of user
                            var filter = Builders<User>.Filter.Eq(x => x.username, username);
                            var invitationsOfUser = user.invitations;
                            invitationsOfUser.Remove(invitation);

                            var update = Builders<User>.Update.Set(x => x.invitations, invitationsOfUser);

                            // Update event
                            var result = collection.UpdateOneAsync(filter, update).Result;
                        }
                        
                    }
                    return (ActionResult)new OkObjectResult(invitations);
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting invitations - " + e.Message);
            }
        }

        [FunctionName("GetTimelineOfUser")]
        public static IActionResult RunGetTimelineOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/timeline")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");
                var eventCollection = database.GetCollection<events.Event>("events");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    var timeline = new List<events.Event>();

                    foreach (String eventID in user.attendingEvents)
                    {
                        bool eventExists = eventCollection.Find(_ => _.eventID == eventID).Any();
                        if (eventExists)
                        {
                            var eve = eventCollection.Find(_ => _.eventID == eventID).First();
                            timeline.Add(eve);
                        }
                        else
                        {
                            // If event is not existing -> remove event from attendingEvents of user
                            var filter = Builders<User>.Filter.Eq(x => x.username, username);
                            var attendingEvents = user.attendingEvents;
                            attendingEvents.Remove(eventID);

                            var update = Builders<User>.Update.Set(x => x.attendingEvents, attendingEvents);

                            // Update event
                            var result = collection.UpdateOneAsync(filter, update).Result;
                        }

                    }
                    return (ActionResult)new OkObjectResult(timeline);
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting timeline - " + e.Message);
            }
        }

        [FunctionName("PutAcceptOrRejectInvitation")]
        public static IActionResult RunPutAcceptOrRejectInvitatio([HttpTrigger(AuthorizationLevel.Function, "put", Route = "User/{username}/invitations/{eventID}/{accept}")]HttpRequest req, String username, String eventID, bool accept, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");
                var eventCollection = database.GetCollection<events.Event>("events");

                bool userExists = collection.Find(_ => _.username == username).Any();
                bool eventExists = eventCollection.Find(_ => _.eventID == eventID).Any();


                if (userExists && eventExists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    var eve = eventCollection.Find(_ => _.eventID == eventID).First();
                    var invitations = new List<events.Event>();


                    // Filter for user: remove invitation from list
                    var filter = Builders<User>.Filter.Eq(x => x.username, username);
                    var invites = user.invitations;
                    invites.Remove(eventID);
                    var monthlyCredit = user.monthlyCredit;
                    var totalCredit = user.totalCredit;

                    var attendingEvents = user.attendingEvents;
                    if (accept)
                    {
                        attendingEvents.Add(eventID);
                        // Add Credit
                        monthlyCredit = monthlyCredit + 5;
                        totalCredit = totalCredit + 5;

                    }
                    var update = Builders<User>.Update.Set(x => x.invitations, invites).Set(x => x.attendingEvents, attendingEvents).Set(_ => _.monthlyCredit, monthlyCredit).Set(_ => _.totalCredit, totalCredit);

                    // Filter for event: remove user from invitedFriends
                    var filterTwo = Builders<events.Event>.Filter.Eq(x => x.eventID, eventID);
                    var invitedFriends = eve.invitedUsers;
                    invitedFriends.Remove(username);

                    // Filter for event: add or ignore user to goingPeople
                    var goingPeople = eve.goingUsers;
                    if (accept)
                    {
                        goingPeople.Add(username);
                    }

                    var updateTwo = Builders<events.Event>.Update.Set(x => x.invitedUsers, invitedFriends)
                        .Set(x => x.goingUsers, goingPeople);


                    // Update event and users
                    var result = collection.UpdateOneAsync(filter, update).Result;
                    var r = eventCollection.UpdateOneAsync(filterTwo, updateTwo).Result;
                    
                    
                    return (ActionResult)new OkObjectResult(invitations);
                }
                else
                {
                    return new BadRequestObjectResult("Username or eventID not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting invitations - " + e.Message);
            }
        }

        [FunctionName("ResetMonthlyCredit")]
        public static void Run([TimerTrigger("0 0 0 1 * *")]TimerInfo myTimer, ILogger log)
        {
            var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
            var database = client.GetDatabase("sukh");
            var collection = database.GetCollection<User>("users");


            var filter = Builders<User>.Filter.Empty;
            var update = Builders<User>.Update.Set(x => x.monthlyCredit, 0);

            // Update users monthlyCredit to 0
            var result = collection.UpdateManyAsync(filter, update).Result;
        }

        [FunctionName("PutReportUser")]
        public static IActionResult RunPutReportUser([HttpTrigger(AuthorizationLevel.Function, "put", Route = "User/{username}/report")] HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();

                    // Check if reports are still valid (not older than three months)
                    List<Report> reports = new List<Report>();
                    DateTime dateThreeMonthsAgo = DateTime.Now.AddMonths(-3);
                    foreach (Report r in user.reports)
                    {
                        DateTime createdAt = r.createdAt;
                        int result = DateTime.Compare(createdAt, dateThreeMonthsAgo);
                        if (result > 0)
                        {
                            reports.Add(r);
                        }

                    }

                    if (reports.Count < 50)
                    {
                        //Fill report
                        var report = new Report
                        {
                            createdBy = "anonym",
                            createdAt = DateTime.Now
                        };


                        reports.Add(report);

                        var filter = Builders<User>.Filter.Eq(x => x.username, username);
                        var update = Builders<User>.Update.Set(x => x.reports, reports);


                        // Update user
                        var result = collection.UpdateOneAsync(filter, update).Result;

                        var res = new OkObjectResult($"MongoDB: sukh.users: Reported {username}");
                        res.StatusCode = StatusCodes.Status200OK;
                        return res;
                    }
                    else
                    {
                        // Delete account with too many (>50 in last three months) reports
                        HttpRequest emptyRequest = null;
                        ILogger emptyLogger = null;
                        RunDeleteUserByName(emptyRequest, username, emptyLogger);

                        var res = new OkObjectResult($"MongoDB: sukh.users: Deleted {username} because of too many reports");
                        res.StatusCode = StatusCodes.Status200OK;
                        return res;
                    }
                }
                else
                {
                    return new BadRequestObjectResult("Username not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error reporting user - " + e.Message);
            }
        }

        [FunctionName("PutBlockUser")]
        public static IActionResult RunPutBlockUser([HttpTrigger(AuthorizationLevel.Function, "put", Route = "User/{username}/block/{blockUsername}")] HttpRequest req, String username, String blockUsername, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();
                bool existsBlockUser = collection.Find(_ => _.username == blockUsername).Any();

                if (exists && existsBlockUser)
                {
                    var user = collection.Find(_ => _.username == username).First();

                    if (user.following.Contains(blockUsername))
                    {
                        // Unfollow user
                        _ = RunRemoveFollower(null, username, blockUsername, null);
                    }

                    if (user.followers.Contains(blockUsername))
                    {
                        // Remove as follower
                        _ = RunRemoveFollower(null, blockUsername, username, null);
                    }

                    List<String> blockedUsers = user.blockedUsers;
                    blockedUsers.Add(blockUsername);

                    var filter = Builders<User>.Filter.Eq(x => x.username, username);
                    var update = Builders<User>.Update.Set(x => x.blockedUsers, blockedUsers);

                    // Update user
                    var result = collection.UpdateOneAsync(filter, update).Result;

                    var res = new OkObjectResult($"MongoDB: sukh.users: Blocked '{blockUsername}' for user '{username}'");
                    res.StatusCode = StatusCodes.Status200OK;
                    return res;
                }
                else
                {
                    return new BadRequestObjectResult("User does not exist");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult($"Error blocking user '{blockUsername}' - " + e.Message);
            }
        }

        [FunctionName("DeleteBlockUser")]
        public static IActionResult RunDeleteBlockUser([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "User/{username}/block/{blockUsername}")] HttpRequest req, String username, String blockUsername, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();

                    List<String> blockedUsers = user.blockedUsers;
                    blockedUsers.Remove(blockUsername);

                    var filter = Builders<User>.Filter.Eq(x => x.username, username);
                    var update = Builders<User>.Update.Set(x => x.blockedUsers, blockedUsers);

                    // Update user
                    var result = collection.UpdateOneAsync(filter, update).Result;

                    var res = new OkObjectResult($"MongoDB: sukh.users: Unblocked '{blockUsername}' for user '{username}'");
                    res.StatusCode = StatusCodes.Status200OK;
                    return res;
                }
                else
                {
                    return new BadRequestObjectResult($"User '{username}' does not exist");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult($"Error unblocking user '{blockUsername}' - " + e.Message);
            }
        }

        [FunctionName("GetBlockedUsers")]
        public static IActionResult RunGetBlockedUsers([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/block")] HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<User>("users");

                bool exists = collection.Find(_ => _.username == username).Any();

                if (exists)
                {
                    var user = collection.Find(_ => _.username == username).First();
                    return (ActionResult)new OkObjectResult(user.blockedUsers);
                }
                else
                {
                    return new BadRequestObjectResult($"User '{username}' does not exist");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult($"Error requesting blocked users of user '{username}' - " + e.Message);
            }
        }


        public class Report
        {
            public string createdBy { get; set; }

            public DateTime createdAt { get; set; }
        }

        public class Notifications
        {
            public int newFollowers { get; set; }

            public int eventInvitations { get; set; }
        }


        // Mongo DB User
        public class User
        {
            [BsonId]
            public ObjectId Id { get; set; }


            [BsonElement("username")]
            public String username { get; set; }

            [BsonElement("name")]
            public string name { get; set; }

            [BsonElement("mail")]
            public String mail { get; set; }

            [BsonElement("password")]
            public String password { get; set; }

            [BsonElement("salt")]
            public String salt { get; set; }

            [BsonElement("createdAt")]
            public DateTime createdAt { get; set; }

            [BsonElement("role")]
            public String role { get; set; }

            [BsonElement("followers")]
            public List<String> followers { get; set; }

            [BsonElement("following")]
            public List<String> following { get; set; }

            [BsonElement("invitations")]
            public List<String> invitations { get; set; }

            [BsonElement("attendingEvents")]
            public List<String> attendingEvents { get; set; }

            [BsonElement("reports")]
            public List<Report> reports { get; set; }

            [BsonElement("blockedUsers")]
            public List<String> blockedUsers { get; set; }

            [BsonElement("profileImageURL")]
            public string profileImageURL { get; set; }

            [BsonElement("monthlyCredit")]
            public int monthlyCredit { get; set; }

            [BsonElement("totalCredit")]
            public int totalCredit { get; set; }

        }

        public static string generateSalt(RNGCryptoServiceProvider RNGcsp, int size)
        {
            var saltByteArray = new Byte[size];
            RNGcsp.GetBytes(saltByteArray);
            return Convert.ToBase64String(saltByteArray);
        }

        public static void setCredits(String username)
        {

        }


        public static async Task<string> uploadImageDataToBlobStorageAsync(byte[] image, string username)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("user-images");
                await cloudBlobContainer.CreateIfNotExistsAsync();
                Uri imageURI = storageAccount.BlobStorageUri.PrimaryUri;

                // Create a fileName and append guid
                string fileName = "ProfileImage_" + username + ".png";

                // Get a reference to the blob address, then upload the file to the blob.
                // Use the value of fileName for the blob name.
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
                int length = image.Length;
                cloudBlockBlob.UploadFromByteArray(image, 0, length);
                return cloudBlobContainer.StorageUri.PrimaryUri + "/" + fileName;
            }
            else
            {
                return "defaulturl";
            }
        }

        public static bool deleteImageDataFromBlobStorage(string username)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {             
                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("user-images");
                string completeFileName = "ProfileImage_" + username + ".png";
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(completeFileName);
                cloudBlockBlob.Delete();

                return true;

            }
            else
            {
                return false;
            }
        }
    }
}

