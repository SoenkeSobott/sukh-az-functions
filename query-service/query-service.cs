using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using Echovoice.JSON;
using System.Collections.Generic;
using MongoDB.Driver.GeoJsonObjectModel;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;

namespace query_service
{

    public static class events
    {
        
        [FunctionName("CreateEvent")]
        public static async Task<IActionResult> RunCreateEvent([HttpTrigger(AuthorizationLevel.Function, "post", Route = "Event")]HttpRequest req, ILogger log)
        {
            string eventID;
            byte[] eventImage;
            string name;
            string type;
            DateTime startTime;
            DateTime endTime;
            List<String> invitedUsers;
            Boolean isPublic;
            double latitude;
            double longitude;
            GeoJsonPoint<GeoJson2DGeographicCoordinates> locationIndex;
            DateTime createdAt;
            string createdBy;
            List<String> tags = new List<String>();

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            eventID = data?.eventID;
            eventImage = data?.eventImage;
            name = data?.name;
            type = data?.type;
            startTime = data?.startTime;
            endTime = data?.endTime;
            invitedUsers = (data?.invitedUsers).ToObject<List<String>>();
            isPublic = data?.isPublic;
            latitude = data?.latitude;
            longitude = data?.longitude;
            createdAt = data?.createdAt;
            createdBy = data?.createdBy;
            tags = (data?.tags).ToObject<List<String>>();

            locationIndex = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(longitude, latitude));

            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                // TODO: maybe like this 
                var userCollection = database.GetCollection<users.User>("users");

                string eventImageURL = await uploadImageDataToBlobStorageAsync(eventImage, eventID);

                //Fill our newEvent
                var newEvent = new Event
                {
                    Id = new ObjectId(),
                    eventID = eventID,
                    eventImageURL = eventImageURL,
                    name = name,
                    type = type,
                    startTime = startTime,
                    endTime = endTime,
                    invitedUsers = invitedUsers,
                    goingUsers = new List<string>(),
                    isPublic = isPublic,
                    latitude = latitude,
                    longitude = longitude,
                    locationIndex = locationIndex,
                    tags = tags,
                    votes = new List<Vote>(),
                    eventFeedEntries = new List<EventFeedEntry>(),
                    createdAt = createdAt,
                    createdBy = createdBy,
                };

                bool exists = collection.Find(_ => _.eventID == newEvent.eventID).Any();

                if (!exists)
                {
                    await collection.InsertOneAsync(newEvent);

                    if (invitedUsers.Count > 0)
                    {
                        // TODO: this is not really correct find better way
                        for (int i = 0; i < invitedUsers.Count; i++)
                        {
                            var username = invitedUsers[i];
                            var userToInvite = userCollection.Find(_ => _.username == username).First();

                            var filter = Builders<users.User>.Filter.Eq(x => x.username, username);

                            // Update requests list of user
                            var updatedInvitations = userToInvite.invitations;
                            if (!updatedInvitations.Contains(username))
                            {
                                updatedInvitations.Add(eventID);
                            }

                            var update = Builders<users.User>.Update.Set(x => x.invitations, updatedInvitations);

                            var t = userCollection.UpdateOneAsync(filter, update).Result;

                        }
                    }
                }
                else
                {
                    return new BadRequestObjectResult("Violating unique constraint - eventID already exists");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error inserting data - " + e.Message);
            }
            var result = new OkObjectResult($"Inserted {name} into db: sukh.events");
            result.StatusCode = StatusCodes.Status201Created;
            return result;
        }

        [FunctionName("UpdateEvent")]
        public static async Task<IActionResult> RunUpdateEvent([HttpTrigger(AuthorizationLevel.Function, "post", Route = "Event/{eventID}")]HttpRequest req, string eventID, ILogger log)
        {
            byte[] eventImage;
            string name;
            string type;
            DateTime startTime;
            DateTime endTime;
            List<String> invitedUsers;
            Boolean isPublic;
            double latitude;
            double longitude;
            GeoJsonPoint<GeoJson2DGeographicCoordinates> locationIndex;
            List<String> tags = new List<String>();

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            eventImage = data?.eventImage;
            name = data?.name;
            type = data?.type;
            startTime = data?.startTime;
            endTime = data?.endTime;
            invitedUsers = (data?.invitedUsers).ToObject<List<String>>();
            isPublic = data?.isPublic;
            latitude = data?.latitude;
            longitude = data?.longitude;
            tags = (data?.tags).ToObject<List<String>>();

            locationIndex = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(longitude, latitude));

            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");
                var userCollection = database.GetCollection<users.User>("users");

                bool exists = collection.Find(_ => _.eventID == eventID).Any();

                if (exists)
                {
                    string eventImageURL = await uploadImageDataToBlobStorageAsync(eventImage, eventID);

                    var existingEvent = collection.Find(_ => _.eventID == eventID).First();
                    var filter = Builders<Event>.Filter.Eq(s => s.eventID, eventID);

                    //Fill updated event
                    var updatedEvent = new Event
                    {
                        Id = existingEvent.Id,
                        eventID = existingEvent.eventID,
                        eventImageURL = eventImageURL,
                        name = name,
                        type = type,
                        startTime = startTime,
                        endTime = endTime,
                        invitedUsers = invitedUsers,
                        goingUsers = existingEvent.goingUsers,
                        isPublic = isPublic,
                        latitude = latitude,
                        longitude = longitude,
                        locationIndex = locationIndex,
                        tags = tags,
                        votes = existingEvent.votes,
                        eventFeedEntries = existingEvent.eventFeedEntries,
                        createdAt = existingEvent.createdAt,
                        createdBy = existingEvent.createdBy,
                    };

                    await collection.ReplaceOneAsync(filter, updatedEvent);

                    // Invite users
                    for (int i = 0; i < invitedUsers.Count; i++)
                    {
                        var username = invitedUsers[i];
                        var userToInvite = userCollection.Find(_ => _.username == username).First();

                        var userFilter = Builders<users.User>.Filter.Eq(x => x.username, username);

                        // Update invitaions of user
                        var updatedInvitations = userToInvite.invitations;
                        if (!updatedInvitations.Contains(eventID))
                        {
                            updatedInvitations.Add(eventID);
                        }

                        var update = Builders<users.User>.Update.Set(x => x.invitations, updatedInvitations);

                        var t = userCollection.UpdateOneAsync(userFilter, update).Result;
                    }

                    // Uninvite users
                    List<String> oldInvitedUsers = existingEvent.invitedUsers;
                    for (int i = 0; i < oldInvitedUsers.Count; i++)
                    {
                        var username = oldInvitedUsers[i];
                        if (!invitedUsers.Contains(username))
                        {
                            var userToUninvite = userCollection.Find(_ => _.username == username).First();

                            var userFilter = Builders<users.User>.Filter.Eq(x => x.username, username);

                            // Update invitaions of user
                            var updatedInvitations = userToUninvite.invitations;
                            if (updatedInvitations.Contains(eventID))
                            {
                                updatedInvitations.Remove(eventID);
                            }

                            var update = Builders<users.User>.Update.Set(x => x.invitations, updatedInvitations);
                            var res = userCollection.UpdateOneAsync(userFilter, update).Result;
                        }
                    }

                    var result = new OkObjectResult($"Updated {eventID} from db: sukh.events");
                    result.StatusCode = StatusCodes.Status200OK;
                    return result;
                }
                else
                {
                    return new BadRequestObjectResult("eventID not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error updating data - " + e.Message);
            }
        }

        [FunctionName("UpdateVotesOfEvent")]
        public static async Task<IActionResult> RunUpdateVotesOfEvent([HttpTrigger(AuthorizationLevel.Function, "post", Route = "Event/{eventID}/votes")]HttpRequest req, String eventID, ILogger log)
        {
            string username;
            Int64 value;
            bool positive;

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            username = data?.username;
            value = data?.value;
            positive = data?.positive;

            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                bool exists = collection.Find(_ => _.eventID == eventID).Any();

                if (exists)
                {
                    var existingEvent = collection.Find(_ => _.eventID == eventID).First();
                    var filter = Builders<Event>.Filter.Eq(s => s.eventID, eventID);

                    var newVote = new Vote
                    {
                        username = username,
                        value = value,
                        positive = positive
                    };
                        
                    var votes = existingEvent.votes;
                    var vote = votes.Find(x => x.username == newVote.username);
                    if (vote != null)
                    {
                        if (vote.value == 1 && vote.positive != newVote.positive)
                        {
                            votes.Remove(vote);
                        } else
                        {
                            var conflictResult = new OkObjectResult($"MongoDB: sukh.events: Failed updating votes of {existingEvent.name} vote already exists");
                            conflictResult.StatusCode = StatusCodes.Status409Conflict;
                            return conflictResult;
                        }
                    } else
                    {
                        votes.Add(newVote);
                    }

                    var updateVotes = Builders<Event>.Update.Set(x => x.votes, votes);
                     
                    // Update event
                    await collection.UpdateOneAsync(filter, updateVotes);

                    var result = new OkObjectResult($"MongoDB: sukh.events: Updated votes of {existingEvent.name}");
                    result.StatusCode = StatusCodes.Status200OK;
                    return result;

                }
                else
                {
                    return new BadRequestObjectResult("eventID not found");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error updating data - " + e.Message);
            }
        }

        [FunctionName("UpdateEventGoingUsers")]
        public static IActionResult RunUpdateEventGoingUsers([HttpTrigger(AuthorizationLevel.Function, "put", Route = "Event/{eventID}/goingUsers/{username}")]HttpRequest req, String eventID, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var userCollection = database.GetCollection<users.User>("users");

                bool eventExists = collection.Find(_ => _.eventID == eventID).Any();
                bool userExists = userCollection.Find(_ => _.username == username).Any();

                var users = new List<users.User>();

                if (eventExists && userExists)
                {
                    Event currentEvent = collection.Find(_ => _.eventID == eventID).First();
                    users.User currentUser = userCollection.Find(_ => _.username == username).First();

                    var goingUsers = currentEvent.goingUsers;
                    if (!goingUsers.Contains(username))
                    {
                        goingUsers.Add(username);
                    }
                    var invitedUsers = currentEvent.invitedUsers;
                    if (invitedUsers.Contains(username))
                    {
                        invitedUsers.Remove(username);
                    }
                    // Filter for event: update goingUsers
                    var eventFilter = Builders<Event>.Filter.Eq(x => x.eventID, eventID);
                    var eventUpdate = Builders<Event>.Update.Set(x => x.goingUsers, goingUsers).Set(x => x.invitedUsers, invitedUsers);

                    var attendingEvents = currentUser.attendingEvents;
                    if (!attendingEvents.Contains(eventID))
                    {
                        attendingEvents.Add(eventID);
                    }

                    var invitations = currentUser.invitations;
                    if (invitations.Contains(eventID))
                    {
                        invitations.Remove(eventID);
                    }
                    var monthlyCredit = currentUser.monthlyCredit;
                    monthlyCredit = monthlyCredit + 5;

                    var totalCredit = currentUser.totalCredit;
                    totalCredit = totalCredit + 5;

                    // Filter for user: update attendingEvents
                    var userFilter = Builders<users.User>.Filter.Eq(x => x.username, username);
                    var userUpdate = Builders<users.User>.Update.Set(x => x.attendingEvents, attendingEvents).Set(x => x.invitations, invitations).Set(_ => _.monthlyCredit, monthlyCredit).Set(_ => _.totalCredit, totalCredit);

                    // Update both users
                    var result = collection.UpdateOneAsync(eventFilter, eventUpdate).Result;
                    var r = userCollection.UpdateOneAsync(userFilter, userUpdate).Result;

                    // return
                    var res = new OkObjectResult($"MongoDB: sukh.users sukh.events: Updated Event {eventID} and User {username}");
                    res.StatusCode = StatusCodes.Status200OK;
                    return res;
                }
                else
                {
                    return new BadRequestObjectResult("EventID or username not exists");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error updating data - " + e.Message);

            }
        }

        [FunctionName("DeleteUserFromEventGoingUsers")]
        public static IActionResult RunDeleteUserFromEventGoingUsers([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "Event/{eventID}/goingUsers/{username}")]HttpRequest req, String eventID, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var userCollection = database.GetCollection<users.User>("users");

                bool eventExists = collection.Find(_ => _.eventID == eventID).Any();
                bool userExists = userCollection.Find(_ => _.username == username).Any();

                var users = new List<users.User>();

                if (eventExists && userExists)
                {
                    Event currentEvent = collection.Find(_ => _.eventID == eventID).First();
                    users.User currentUser = userCollection.Find(_ => _.username == username).First();

                    var goingUsers = currentEvent.goingUsers;
                    if (goingUsers.Contains(username))
                    {
                        goingUsers.Remove(username);
                    }
                    
                    // Filter for event: update goingUsers
                    var eventFilter = Builders<Event>.Filter.Eq(x => x.eventID, eventID);
                    var eventUpdate = Builders<Event>.Update.Set(x => x.goingUsers, goingUsers);

                    var attendingEvents = currentUser.attendingEvents;
                    if (attendingEvents.Contains(eventID))
                    {
                        attendingEvents.Remove(eventID);
                    }

                    var monthlyCredit = currentUser.monthlyCredit;
                    monthlyCredit = monthlyCredit - 5;

                    var totalCredit = currentUser.totalCredit;
                    totalCredit = totalCredit - 5;

                    // Filter for user: update attendingEvents
                    var userFilter = Builders<users.User>.Filter.Eq(x => x.username, username);
                    var userUpdate = Builders<users.User>.Update.Set(x => x.attendingEvents, attendingEvents).Set(_ => _.monthlyCredit, monthlyCredit).Set(_ => _.totalCredit, totalCredit);

                    // Update both users
                    var result = collection.UpdateOneAsync(eventFilter, eventUpdate).Result;
                    var r = userCollection.UpdateOneAsync(userFilter, userUpdate).Result;

                    // return
                    var res = new OkObjectResult($"MongoDB: sukh.users sukh.events: Updated Event {eventID} and User {username}");
                    res.StatusCode = StatusCodes.Status200OK;
                    return res;
                }
                else
                {
                    return new BadRequestObjectResult("EventID or username not exists");
                }

            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error updating data - " + e.Message);
            }
        }


        [FunctionName("GetEvent")]
        public static IActionResult RunGetEvent([HttpTrigger(AuthorizationLevel.Function, "get", Route = "Event/{id}")]HttpRequest req, String id, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var returnEvent = collection.Find(_ => _.eventID == id).First();

                return (ActionResult)new OkObjectResult(returnEvent);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetEventsBySearchTerm")]
        public static IActionResult RunGetEventsBySearchTerm([HttpTrigger(AuthorizationLevel.Function, "get", Route = "Event/search/{requestingUsername}/{searchTerm}")]HttpRequest req, String requestingUsername, String searchTerm, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");
                var userCollection = database.GetCollection<users.User>("users");

                bool exists = userCollection.Find(_ => _.username == requestingUsername).Any();

                if (exists)
                {
                    var user = userCollection.Find(_ => _.username == requestingUsername).First();

                    var filter = Builders<Event>.Filter.Regex(_ => _.name, new BsonRegularExpression(searchTerm, "i"));
                    // Check that event is not from an blocked user
                    filter = filter & Builders<Event>.Filter.Nin(_ => _.createdBy, user.blockedUsers);

                    var matchingEvents = collection.Find(filter).ToList();

                    return (ActionResult)new OkObjectResult(matchingEvents);
                }
                else
                {
                    return new BadRequestObjectResult($"Requesting user '{requestingUsername}' does not exist.");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("DeleteEvent")]
        public static IActionResult RunDeleteEvent([HttpTrigger(AuthorizationLevel.Function, "delete", Route = "Event/{eventID}")]HttpRequest req, String eventID, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");
                var userCollection = database.GetCollection<users.User>("users");

                bool exists = collection.Find(_ => _.eventID == eventID).Any();

                if (exists)
                {
                    var currentEvent = collection.Find(_ => _.eventID == eventID).First();

                    // Delete connected data from users
                    var invitedUsers = currentEvent.invitedUsers;
                    var goingUsers = currentEvent.goingUsers;
                    var eventFeedEntries = currentEvent.eventFeedEntries;

                    foreach (String username in invitedUsers)
                    {
                        var user = userCollection.Find(_ => _.username == username).First();
                        // Filter for updating user: remove event
                        var filter = Builders<users.User>.Filter.Eq(x => x.username, username);
                        var invitations = user.invitations;

                        if (invitations.Contains(eventID))
                        {
                            invitations.Remove(eventID);
                        }
                        
                        var update = Builders<users.User>.Update.Set(x => x.invitations, invitations);

                        // Update user
                        var result = userCollection.UpdateOneAsync(filter, update).Result;
                    }

                    foreach (String username in goingUsers)
                    {
                        var user = userCollection.Find(_ => _.username == username).First();
                        // Filter for updating user: remove event
                        var filter = Builders<users.User>.Filter.Eq(x => x.username, username);
                        var attendingEvents = user.attendingEvents;

                        if (attendingEvents.Contains(eventID))
                        {
                            attendingEvents.Remove(eventID);
                        }
                        var update = Builders<users.User>.Update.Set(x => x.attendingEvents, attendingEvents);

                        // Update user
                        var result = userCollection.UpdateOneAsync(filter, update).Result;
                    }

                    foreach (EventFeedEntry entry in eventFeedEntries)
                    {
                        eventFeeds.deleteImageDataFromBlobStorage(entry.feedEntryId);
                    }

                    // Delete image data
                    deleteImageDataFromBlobStorage(eventID);
                    // Delete event from Mongo
                    collection.DeleteOne(_ => _.eventID == eventID);
                    // TODO: display in message what user data was 'cascade' deleted
                    return (ActionResult)new OkObjectResult($"Deleted event from db: sukh.events");

                }
                else
                {
                    return new BadRequestObjectResult("Error deleting data - Passed eventID does not exist");
                }
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error deleting data - " + e.Message);
            }
        }

        [FunctionName("GetAllEvents")]
        public static async Task<IActionResult> RunGetAllEvent([HttpTrigger(AuthorizationLevel.Function, "get", Route = "Event/r")]HttpRequest req, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var events = await collection.Find(_ => true).ToListAsync();

                return (ActionResult)new OkObjectResult(events);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetAllEventsOfUser")]
        public static async Task<IActionResult> RunGetAllEventsOfUser([HttpTrigger(AuthorizationLevel.Function, "get", Route = "User/{username}/events")]HttpRequest req, String username, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var eventsOfUser = await collection.Find(_ => _.createdBy == username).ToListAsync();

                return (ActionResult)new OkObjectResult(eventsOfUser);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetEventsFiltered")]
        public static async Task<IActionResult> RunGetEventsFiltered([HttpTrigger(AuthorizationLevel.Function, "post", Route = "Event/all/filter")]HttpRequest req, ILogger log)
        {
            // Geo filter
            double latitude;
            double longitude;
            double radius;

            // Time filter
            DateTime? from;
            DateTime? to;

            // Tag filter
            List<String> tags;

            // Type filter
            List<String> types;
            
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);

            latitude = data?.latitude;
            longitude = data?.longitude;
            radius = data?.radius;

            from = data?.from;
            to = data?.to;
            tags = (data?.tags).ToObject<List<String>>();
            types = (data?.types).ToObject<List<String>>();

            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                // Geo filter
                var point = GeoJson.Point(GeoJson.Geographic(longitude, latitude));
                var filter = Builders<Event>.Filter.NearSphere(x => x.locationIndex, point, radius);

                // Time filter
                if (from.Equals(null) && !to.Equals(null))
                {
                    // Previous events
                    filter = filter & Builders<Event>.Filter.Where(x => x.endTime < DateTime.Now);
                }
                else if (to.Equals(null) && !from.Equals(null))
                {
                    // Upcoming events
                    filter = filter & Builders<Event>.Filter.Where(x => x.endTime > DateTime.Now);
                }
                else
                {
                    // Events in specific time range
                    filter = filter & Builders<Event>.Filter.Where(x => x.endTime > from) & Builders<Event>.Filter.Where(x => x.startTime < to);
                }

                // Tag filter

                // Type filter

                var events = await collection.Find(filter).ToListAsync();

                return (ActionResult)new OkObjectResult(events);

            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        public enum Filter
        {
            Upcoming,
            Previous
        }

        [FunctionName("GetAllUpcomingEventsInRadius")]
        public static async Task<IActionResult> RunGetAllUpcomingEventsInRadius([HttpTrigger(AuthorizationLevel.Function, "get", Route = "Event/{lat}/{lon}/{radius}/{eventFilter}")] HttpRequest req, Double lat, Double lon, int radius, String eventFilter, ILogger log)
        {
            try
            {
                Enum.TryParse(eventFilter, out Filter myFilter);
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var point = GeoJson.Point(GeoJson.Geographic(lon, lat));
                var now = DateTime.Now;

                var filter = Builders<Event>.Filter.NearSphere(x => x.locationIndex, point, radius);
                if (myFilter == Filter.Upcoming)
                {
                    filter = filter & Builders<Event>.Filter.Where(x => x.endTime > now);
                }
                else
                {
                    filter = filter & Builders<Event>.Filter.Where(x => x.endTime < now);
                }
                var events = await collection.Find(filter).ToListAsync();

                return (ActionResult)new OkObjectResult(events);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        [FunctionName("GetGoingUsersOfEvent")]
        public static async Task<IActionResult> RunGetGoingUsersOfEvent([HttpTrigger(AuthorizationLevel.Function, "get", Route = "Event/{eventID}/goingUsers")]HttpRequest req, String eventID, ILogger log)
        {
            try
            {
                var client = new MongoClient(System.Environment.GetEnvironmentVariable("MongoDBAtlasConnectionString"));
                var database = client.GetDatabase("sukh");
                var collection = database.GetCollection<Event>("events");

                var userCollection = database.GetCollection<users.User>("users");

                bool exists = collection.Find(_ => _.eventID == eventID).Any();
                var users = new List<users.User>();

                if (exists)
                {
                    Event currentEvent = collection.Find(_ => _.eventID == eventID).First();

                    var goingUsers = currentEvent.goingUsers;
                    if (goingUsers.Count > 0)
                    {
                        for (int i = 0; i < goingUsers.Count; i++)
                        {
                            var username = goingUsers[i];
                            users.User user = userCollection.Find(_ => _.username == username).First();

                            users.Add(user);
                        }
                    }
                    return (ActionResult)new OkObjectResult(users);
                }
                else
                {
                    return new BadRequestObjectResult("EventID not exists");
                }

            }
            catch (Exception e)
            {
                return new BadRequestObjectResult("Error requesting data - " + e.Message);
            }
        }

        public class Event
        {
            [BsonId]
            public ObjectId Id { get; set; }

            [BsonElement("eventID")]
            public String eventID { get; set; }

            [BsonElement("eventImageURL")]
            public string eventImageURL { get; set; }

            [BsonElement("name")]
            public string name { get; set; }

            [BsonElement("type")]
            public String type { get; set; }

            [BsonElement("startTime")]
            public DateTime startTime { get; set; }

            [BsonElement("endTime")]
            public DateTime endTime { get; set; }

            [BsonElement("invitedUsers")]
            public List<String> invitedUsers { get; set; }

            [BsonElement("goingUsers")]
            public List<String> goingUsers { get; set; }

            [BsonElement("isPublic")]
            public Boolean? isPublic { get; set; }

            [BsonElement("latitude")]
            public Double? latitude { get; set; }

            [BsonElement("longitude")]
            public Double? longitude { get; set; }

            [BsonElement("locationIndex")]
            public GeoJsonPoint<GeoJson2DGeographicCoordinates> locationIndex { get; set; }

            [BsonElement("tags")]
            public List<String> tags { get; set; }

            [BsonElement("votes")]
            public List<Vote> votes { get; set; }

            [BsonElement("eventFeedEntries")]
            public List<EventFeedEntry> eventFeedEntries { get; set; }

            [BsonElement("createdAt")]
            public DateTime createdAt { get; set; }

            [BsonElement("createdBy")]
            public String createdBy { get; set; }

        }

        public class Vote
        {
            [BsonElement("voter")]
            public string username { get; set; }

            [BsonElement("value")]
            public Int64 value { get; set; }

            [BsonElement("positive")]
            public Boolean positive { get; set; }
        }

        // Event feed entry
        public class EventFeedEntry
        {
            [BsonElement("feedEntryId")]
            public String feedEntryId { get; set; }

            [BsonElement("imageURL")]
            public String imageURL { get; set; }

            [BsonElement("createdBy")]
            public String createdBy { get; set; }

            [BsonElement("createdAt")]
            public DateTime createdAt { get; set; }
        }

        public static async Task<string> uploadImageDataToBlobStorageAsync(byte[] image, string eventID)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                // If the connection string is valid, proceed with operations against Blob
                // storage here.

                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("event-images");
                await cloudBlobContainer.CreateIfNotExistsAsync();
                Uri imageURI = storageAccount.BlobStorageUri.PrimaryUri;

                // Create a fileName and append guid
                string fileName = "eventImage_" + eventID + ".png";

                // Get a reference to the blob address, then upload the file to the blob.
                // Use the value of fileName for the blob name.
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
                int length = image.Length;
                cloudBlockBlob.UploadFromByteArray(image, 0, length);
                return cloudBlobContainer.StorageUri.PrimaryUri + "/" + fileName;
            }
            else
            {
                return "defaulturl";
            }
        }

        public static bool deleteImageDataFromBlobStorage(string eventID)
        {
            string storageConnectionString = System.Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                // If the connection string is valid, proceed with operations against Blob
                // storage here.

                // Create the CloudBlobClient that represents the 
                // Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                // Create a container called 'quickstartblobs' and 
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("event-images");
                string completeFileName = "eventImage_" + eventID + ".png";
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(completeFileName);
                cloudBlockBlob.Delete();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

